============================================================================
SUMMARY
============================================================================
Submenu blocks

============================================================================
REQUIREMENTS
============================================================================

none.

============================================================================
How to use it?
============================================================================

Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7 for further information.

Configuration
  No configuration needed.

Usage
  You can simply add the blocks at your favored regions.
  After that click on the pictures for happiness.
